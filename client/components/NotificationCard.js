import React from 'react';
import { Feed } from 'semantic-ui-react';
import styled from 'styled-components';

const StyledNotificationCardUnread = styled(Feed)`
    background: #EBEFF2 !important;
    height: 70px !important; 
    margin: 0px !important; 
    borderBottom: 1px solid #D6DFEE !important; 
    padding: 10px !important;
`;

const StyledNotificationCardRead = styled(Feed)`
    background: #F8F8FF !important;
    height: 70px !important; 
    margin: 0px !important; 
    borderBottom: 1px solid #D6DFEE !important; 
    padding: 10px !important;
`;

class NotificationCard extends React.Component {
    
    render() {

        const { notification } = this.props;

        let returnedFeedComponent = (<StyledNotificationCardRead>
            <Feed.Event>
              <Feed.Label image='/assets/images/avatar/small/jenny.jpg' />
              <Feed.Content>
                <Feed.Date content={notification.title} />
                <Feed.Summary>
                  {notification.body}
                </Feed.Summary>
              </Feed.Content>
            </Feed.Event>
            </StyledNotificationCardRead>);

        if(!notification.is_read){
            returnedFeedComponent = (<StyledNotificationCardUnread>
                <Feed.Event>
                  <Feed.Label image='/assets/images/avatar/small/jenny.jpg' />
                  <Feed.Content>
                    <Feed.Date content={notification.title} />
                    <Feed.Summary>
                      {notification.body}
                    </Feed.Summary>
                  </Feed.Content>
                </Feed.Event>
                </StyledNotificationCardUnread>);
        }

        return returnedFeedComponent;
    }
}
  
  export default NotificationCard;