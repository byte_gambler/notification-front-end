/*
    ./client/components/App.jsx
*/
import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { Menu, Button, Icon, Popup, Dropdown, Image, Card, Label, Input, Grid } from 'semantic-ui-react';
import styled from 'styled-components';
import NotificationBadge from 'react-notification-badge';
import {Effect} from 'react-notification-badge';
import NotificationList from './NotificationList';
import SearchExampleInput from './SearchExampleInput.jsx';
import NotificationCard from './NotificationCard.js';
import { startSetNotifications, markAllNotificationsRead } from '../actions/notification.js';

//allow react dev tools work
window.React = React;
const trigger = (
    <span>
        <Image avatar src='static/images/man.png' />
    </span>
  )
  
  const options = [
    {
      key: 'user',
      text: <span>Signed in as <strong>Bob Smith</strong></span>,
      disabled: true,
    },
    { key: 'profile', text: 'Your Profile' },
    { key: 'stars', text: 'Your Stars' },
    { key: 'explore', text: 'Explore' },
    { key: 'integrations', text: 'Integrations' },
    { key: 'help', text: 'Help' },
    { key: 'settings', text: 'Settings' },
    { key: 'sign-out', text: 'Sign Out' },
  ];

const StyledImage = styled(Image)`
  margin-top: -16px !important;
`;

const StyledMenuItem = styled(Menu.Item)`
  background: #A3B5C2 !important;
  width: 100px !important;
`;

const StyledInputSearch = styled(Input)`
  width: 400px !important;
  border-radius: 10px !important;
  height:50px !important;
`;

const StyledNotificationLabel = styled(Label)`
  position: relative !important;
  border-radius: 1000px !important;
  top: -36px !important;
  width: 24px !important;
  height: 24px !important;
`;

const StyledRecentActivityLabel = styled(Label)`
    position: relative !important;
    border-radius: 500px !important;
    width: 24px !important;
    height: 24px !important;
    margin-left: 10px !important;
`;

const StyledPopup = styled(Popup)`
  max-height: 300px !important;
  overflow: auto !important;
  padding: 0px !important;
`;

class App extends React.Component {

    constructor() {
        super();
    
        this.state = {
          count:1,
          isLoading: false,
            results: [],
            value: '',

            notifications: [
                {
                  id: 1,
                  title: 'Notification 1',
                  body: 'Notification 1 body',
                  is_read: false
                },
                {
                  id: 2,
                  title: 'Notification 2',
                  body: 'Notification 2 body',
                  is_read: true
                },
                {
                  id: 3,
                  title: 'Notification 3',
                  body: 'Notification 3 body',
                  is_read: true
                },
                {
                  id: 4,
                  title: 'Notification 4',
                  body: 'Notification 4 body',
                  is_read: false
                },
                {
                  id: 5,
                  title: 'Notification 5',
                  body: 'Notification 5 body',
                  is_read: false
                },
                {
                  id: 6,
                  title: 'Notification 6',
                  body: 'Notification 6 body',
                  is_read: false
                },
                {
                  id: 7,
                  title: 'Notification 7',
                  body: 'Notification 7 body',
                  is_read: false
                },
                {
                  id: 8,
                  title: 'Notification 8',
                  body: 'Notification 8 body',
                  is_read: false
                },
                {
                  id: 9,
                  title: 'Notification 9',
                  body: 'Notification 9 body',
                  is_read: true
                },
                {
                  id: 10,
                  title: 'Notification 10',
                  body: 'Notification 10 body',
                  is_read: true
                },
                {
                  id: 11,
                  title: 'Notification 11',
                  body: 'Notification 11 body',
                  is_read: true
                },
                {
                  id: 12,
                  title: 'Notification 12',
                  body: 'Notification 12 body',
                  is_read: true
                },
                {
                  id: 13,
                  title: 'Notification 13',
                  body: 'Notification 13 body',
                  is_read: true
                }
            ]

        };
        this.markAllNotificationsRead = this.markAllNotificationsRead.bind(this);
    }

    componentWillMount() {
        setInterval(() => {
            this.props.startSetNotifications();
        }, 2000);
    }

    markAllNotificationsRead() {
        const unreadIds = this.props.notifications.filter(notification => !notification.is_read).map(notification => notification.id);
        if (unreadIds.length) {
            this.props.markAllNotificationsRead(unreadIds);
        }
    }

    render() {
        const { isLoading, value, results } = this.state;
        const { notifications } = this.props;        
        const unreadNotificationCount = notifications.filter(notification => !notification.is_read).length;
        const notificationListElement = notifications.map(notification => <NotificationCard key={notification.id} notification={notification}/>  );
        let triggerComponent = (<Icon name='alarm outline' size='large' circular >
            <StyledNotificationLabel style={{background: '#59B8B9'}} floating>{unreadNotificationCount}</StyledNotificationLabel>
        </Icon>);
        if (!unreadNotificationCount) {
            triggerComponent = (<Icon name='alarm outline' size='large' circular />);
        }

        let NotificationPopupElement = (
            <StyledPopup
                trigger={triggerComponent}
                position='bottom center'
                on='click'
                onClose={this.markAllNotificationsRead}
            >
                <Card style={{margin: '0px'}}>
                    <Card.Content className='center aligned'>
                        
                            <Card.Header>Notifications {unreadNotificationCount!==0 ? <StyledRecentActivityLabel style={{background: '#59B8B9'}}>{unreadNotificationCount}</StyledRecentActivityLabel> : ''}</Card.Header>
                      
                    </Card.Content>
                </Card>
                {notificationListElement}
            </StyledPopup>
        );

        let MessagePopupElement = (<Popup
            trigger={<Icon name='mail outline' size='large' circular />}
            position='bottom center'
            on='click'
            content='No new messages.'
        />
        );

        return (
            <div>
                <Menu>
                    <Menu.Item>
                    <StyledInputSearch
                        icon='search'
                        iconPosition='left'
                        placeholder='Search...'
                    />
                    </Menu.Item>

                    <Menu.Item position='right'>
                        {NotificationPopupElement}
                        {MessagePopupElement}
                    </Menu.Item>

                    <StyledMenuItem position='right'>
                        <Dropdown trigger={trigger} options={options} pointing='top right' />
                    </StyledMenuItem>
                </Menu>
            
                <StyledImage src='static/images/showcase.jpg' fluid />
                    
            </div>
        );
    }
}

const mapStateToProps = state => ({
    notifications: state.notificationList.notificationList,
});

const mapDispatchToProps = dispatch => ({
    startSetNotifications: () => dispatch(startSetNotifications()),
    markAllNotificationsRead: () => dispatch(markAllNotificationsRead()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);