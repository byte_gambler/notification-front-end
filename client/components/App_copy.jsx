/*
    ./client/components/App.jsx
*/
import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import NotificationBadge from 'react-notification-badge';
import {Effect} from 'react-notification-badge';
import NotificationList from './NotificationList';
import { Input, Menu } from 'semantic-ui-react';

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

//allow react dev tools work
window.React = React;

export default class App extends React.Component {

    constructor() {
        super();
    
        this.state = {
          modalIsOpen: false,
          count:1,
          notifications: [
              {
                id: 1,
                title: 'Notification 1',
                body: 'Notification 1 body'
              },
              {
                id: 2,
                title: 'Notification 2',
                body: 'Notification 2 body'
              },
              {
                id: 3,
                title: 'Notification 3',
                body: 'Notification 3 body'
              },
              {
                id: 4,
                title: 'Notification 4',
                body: 'Notification 4 body'
              },
              {
                id: 5,
                title: 'Notification 5',
                body: 'Notification 5 body'
              },
              {
                id: 6,
                title: 'Notification 6',
                body: 'Notification 6 body'
              },
              {
                id: 7,
                title: 'Notification 7',
                body: 'Notification 7 body'
              },
              {
                id: 8,
                title: 'Notification 8',
                body: 'Notification 8 body'
              },
              {
                id: 9,
                title: 'Notification 9',
                body: 'Notification 9 body'
              },
              {
                id: 10,
                title: 'Notification 10',
                body: 'Notification 10 body'
              },
              {
                id: 11,
                title: 'Notification 11',
                body: 'Notification 11 body'
              },
              {
                id: 12,
                title: 'Notification 12',
                body: 'Notification 12 body'
              },
              {
                id: 13,
                title: 'Notification 13',
                body: 'Notification 13 body'
              }
          ]
        };
    
        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }
    
    openModal() {
    this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
    // references are now sync'd and can be accessed.
    console.log(this.subtitle);
    this.subtitle.color = '#f00';
    }

    closeModal() {
    this.setState({modalIsOpen: false});
    }

    render() {
        return (
            <div>
                <NotificationBadge count={this.state.count} effect={Effect.ROTATE_X}/>
                <button onClick={this.openModal}>Open Modal</button>
                <Menu>
                    <Menu.Item>
                    <Input className='icon' icon='search' placeholder='Search...' />
                    </Menu.Item>
                
                    <Menu.Item position='right'>
                    <Input action={{ type: 'submit', content: 'Go' }} placeholder='Navigate to...' />
                    </Menu.Item>
                </Menu>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >
                <NotificationList
                    ref={subtitle => this.subtitle = subtitle}
                    notifications={this.state.notifications}
                />
                </Modal>
                <Menu>
                <Menu.Item>
                    <SearchExampleInput />
                </Menu.Item>
            
                <Menu.Item position='right'>
                    <SearchExampleInput />
                </Menu.Item>
            </Menu>
            <Popup
            trigger={<Icon name='alarm outline' size='large' circular />}
            position='bottom left'
        >
            <NotificationList 
                notifications={this.state.notifications}
            />
        </Popup>
            </div>
        );
    }
}