const notificationReducerDefaultState = {
    notificationList: [],
    lastOpened: '',
};

const notificationReducers = (state = notificationReducerDefaultState, action) => {
	switch(action.type) {
	case 'ADD_NOTIFICATION':
        return [...state, action.notification];
    case 'SET_NOTIFICATION': {
        const { notifications } = action;
        return Object.assign({}, state, { notificationList: notifications });
    }
    case 'MARK_ALL_READ': {
        const modifiedNotificationList = state.notificationList.map(notification => Object.assign({}, notification, { is_read: true }));
        return Object.assign({}, state, { notificationList: modifiedNotificationList });
    }
	default:
		return state;
	}
};

export default notificationReducers;